#!/bin/bash

source /etc/parallelcluster/cfnconfig

case "${cfn_node_type}" in
    MasterServer)
        cp -rp /shared/config/{.spack,.bashrc,.tmux.conf,.gitconfig,.conda,.condarc} /home/ubuntu
        cp -rp /shared/config/git_id_rsa /home/ubuntu/.ssh
    ;;
    ComputeFleet)
    ;;
    *)
    ;;
esac
